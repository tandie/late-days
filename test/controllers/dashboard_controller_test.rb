require 'test_helper'

class DashboardControllerTest < ActionDispatch::IntegrationTest

  def setup
    @base_title = "Late days"
  end

  test "should get view" do
    get home_url
    assert_response :success
    assert_select "title", "Plachta | #{@base_title}"
  end

end
