require 'test_helper'

class StudentsControllerTest < ActionDispatch::IntegrationTest

	def setup
    	@student = students(:one)
	end

	teardown do
		Rails.cache.clear
	end

	test "should list students" do
		get students_url
		# assert_response :success
		# assert_select 'h1', 'Študenti'
	end

	test "should create student" do
	   	assert_difference('Student.count') do
	   		post students_url, params: { student: { name: "Arnošt Kábel", ais_id: 12345 } } 
		end
	    assert_redirected_to students_path
	end

	test "should destroy student" do
		assert_difference('Student.count', -1) do
	    	delete student_url(@student)
		end

		assert_redirected_to students_path
	end

	test "should update student" do
	  	patch student_url(@student), params: { student: { name: "update" } }
	   	assert_redirected_to students_path
	  	@student.reload
	    assert_equal "update", @student.name
	end

end
