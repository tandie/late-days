require 'test_helper'

class CoursesSemesterControllerTest < ActionDispatch::IntegrationTest

	def setup
		@courses_semester = courses_semesters(:one)
		@semester = semesters(:one)
		@course = courses(:one)
		@labs = labs(:one)
	end

	teardown do
		Rails.cache.clear
	end

	test "should show courses_semester" do
	    get courses_semester_url(@courses_semester)
	    assert_response :success
	    assert_select 'h1', @course.name
	end

	test "should create course" do
	    assert_difference('CoursesSemester.count') do
	      	post courses_semesters_url, params: { 
	        	abbr: @course.abbr,
	        	name: @course.name,
	        	semester_id: @semester, 
	        	course_id: @course,
	        	supervisor: "Branislav Steinmüller",
	        	late_days: 5 } 
	    	end
	    assert_redirected_to courses_semester_path(CoursesSemester.last)
	end

end