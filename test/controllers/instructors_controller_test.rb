require 'test_helper'

class InstructorControllerTest < ActionDispatch::IntegrationTest

  def setup
    @instructor = instructors(:one)
  end

  teardown do
    Rails.cache.clear
  end

  test "should list instructors" do
    get instructors_url
    assert_response :success
    assert_select 'h1', 'Cvičiaci'
  end

  test "should create instructor" do
    assert_difference('Instructor.count') do
      post instructors_url, params: { instructor: { name: "Michal Priemerny", ais_id: 123456 } } 
    end

    assert_redirected_to instructors_path
  end

  test "should destroy instructor" do
    assert_difference('Instructor.count', -1) do
      delete instructor_url(:one)
    end

    assert_redirected_to instructors_path
  end

  test "should update instructor" do
    patch instructor_url(@instructor), params: { instructor: { name: "Michal" } }
    assert_redirected_to instructors_path
    @instructor.reload
   assert_equal "Michal", @instructor.name
  end
end
