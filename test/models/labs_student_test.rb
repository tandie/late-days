require 'test_helper'

class LabsStudentTest < ActiveSupport::TestCase

	test "should not save empty labs_student entry" do
		lab_student = LabsStudent.new
		assert_not lab_student.save
	end

end
