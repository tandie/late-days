require 'test_helper'

class CourseTest < ActiveSupport::TestCase
  
	def setup
		@course = courses(:one)
		@course2 = courses(:two)
	end

	test "should not save empty course" do
		course = Course.new
		assert_not course.save
	end

	test "should be valid" do
    	assert @course.valid?
  end

  test "name should be present" do
    @course.name = "   "
    assert_not @course.valid?
  end

  test "name should be unique" do
  	@course.save
  	@course2.name = @course.name
  	assert_not @course2.save
  end

  test "abbr should be present" do
    @course.abbr = "  "
    assert_not @course.save
  end

  test "abbr should be unique" do
    @course.save
    @course2.abbr = @course.abbr
    assert_not @course2.save
  end

end
