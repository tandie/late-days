require 'test_helper'

class CoursesSemesterTest < ActiveSupport::TestCase
  
	def setup
		@courses_semester = courses_semesters(:one)
	end

	test "should not save empty record" do
		courses_semester = CoursesSemester.new
		assert_not courses_semester.save
	end

	test "course_id shoul be present" do
		@courses_semester.course_id = nil
		assert_not @courses_semester.valid?
		assert_not @courses_semester.save
	end

	test "semester_id should be present" do
		@courses_semester.semester_id = nil
		assert_not @courses_semester.valid?
		assert_not @courses_semester.save
	end

	test "supervisor could be empty" do
		@courses_semester.supervisor = "  "
		assert @courses_semester.valid?
		assert @courses_semester.save
	end

	test "supervisor should not be number" do
		@courses_semester.supervisor = "123456"
		assert_not is_number? @courses_semester.supervisor
	end

	test "courses_semester should be valid" do
		assert @courses_semester.valid?
		assert @courses_semester.save
	end

	test "late_days should be number or nil" do
		@courses_semester.late_days = " "
		assert @courses_semester.valid?
		assert @courses_semester.save
		
		@courses_semester.late_days = "string"
		assert_not @courses_semester.valid?
		assert_not @courses_semester.save
		
		@courses_semester.late_days = 7
		assert @courses_semester.valid?
		assert @courses_semester.save
	end

	test "late_days should be between 1 and 100" do
		@courses_semester.late_days = 0
		assert_not @courses_semester.valid?
		assert_not @courses_semester.save

		@courses_semester.late_days = 101
		assert_not @courses_semester.valid?
		assert_not @courses_semester.save

		@courses_semester.late_days = 7
		assert @courses_semester.valid?
		assert @courses_semester.save
	end

end