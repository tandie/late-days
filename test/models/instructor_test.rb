require 'test_helper'

class InstructorTest < ActiveSupport::TestCase
  
	def setup
		@instructor = instructors(:one)
		@instructor2 = instructors(:two)
	end

	test "instructor should not be empty" do
		instructor = Instructor.new
		assert_not instructor.valid?
	end

	test "name should be present" do
		@instructor.name = "   "
		assert_not @instructor.valid?
	end

	test "ais_id should be present" do
		@instructor.ais_id = nil
		assert_not @instructor.valid?
	end

	test "ais_id should be number" do
		@instructor.ais_id = String.new
		assert_not @instructor.valid?
	end

	test "name should not be number" do
		@instructor.name = 12345
		assert_not @instructor.valid?
	end

	test "ais_id should be unique" do
		@instructor.save
		@instructor2.ais_id = @instructor.ais_id
		assert_not @instructor2.valid?
	end

end
