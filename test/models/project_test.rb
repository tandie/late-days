require 'test_helper'

class ProjectTest < ActiveSupport::TestCase

	def setup
		@project = projects(:one)
	end

	test "empty project should not be valid" do
		project = Project.new
		assert_not project.valid?
	end

	test "should not save empty project" do
		project = Project.new
		assert_not project.save
	end

	test "project without assignment_id should not be valid" do
		@project.assignment_id = nil
		assert_not @project.valid?
	end

	test "should not save project without assignment_id" do
		@project.assignment_id = nil
		assert_not @project.save
	end

	test "project without courses_semester_id should not be valid" do
		@project.courses_semester_id = nil
		assert_not @project.valid?
	end

	test "should not save project without courses_semester_id" do
		@project.courses_semester_id = nil
		assert_not @project.save
	end

	test "deadline should be valid date" do
		deadline = @project.deadline
		assert Date.valid_date?(deadline.year, deadline.month, deadline.day)
	end

	test "empty deadline should not be valid" do
		deadline = @project.deadline = " "
		assert_not Date.valid_date?(deadline.year, deadline.month, deadline.day) if deadline.present?
	end

	test "success should be number from 0 to 100 or nil" do
		@project.success = -1
		assert_not @project.valid?
		@project.success = 101
		assert_not @project.valid?
		@project.success = "  "
		assert @project.valid?
		@project.success = 100
		assert @project.valid?
		@project.success = 0
		assert @project.valid?
	end

end
