require 'test_helper'

class AssignmentTest < ActiveSupport::TestCase
  
	def setup
		@assignment = assignments(:one)
	end

	test "empty assignment should not be valid" do
		@assignment = Assignment.new
		assert_not @assignment.valid?
	end

	test "assignment should be valid" do
		assert @assignment.valid?
	end

	test "name should be present" do
		@assignment.name = "   "
		assert_not @assignment.valid?
	end

	test "should not save empty assignment" do
		assignment = Assignment.new
		assert_not assignment.save
	end

	test "should not save assignment without course_id" do
		@assignment.course_id = nil
		assert_not @assignment.save
	end

end
