require 'test_helper'

class LabTest < ActiveSupport::TestCase

	def setup
		@lab = labs(:one)
	end

	test "empty lab should not be valid" do
		lab = Lab.new
		assert_not lab.valid?
		assert_not lab.save
	end

	test "instructord_id should be present" do
		@lab.instructor_id = nil
		assert_not @lab.valid?
	end

	test "courses_semester_id should be present" do
		@lab.courses_semester_id = nil
		assert_not @lab.valid?
	end

	test "start_at should be present" do
		@lab.start_at = "  "
		assert_not @lab.valid?
	end

	test "duration should be present" do
		@lab.duration = "  "
		assert_not @lab.valid?
	end

	test "day should be present" do
		@lab.day = "  "
		assert_not @lab.valid?
	end

	test "day should be between 0 and 4" do
		@lab.duration = -1
		assert_not @lab.valid?

		@lab.duration = 7
		assert_not @lab.valid?
	end

	test "start_at should be time" do
		@lab.start_at = 12345
		assert_not @lab.valid?

		@lab.start_at = 7.00
		assert_not @lab.valid?

		@lab.start_at = 2017-06-28
		assert_not @lab.valid?
	end

	test "duration should be integer" do
		@lab.duration = "string"
		assert_not @lab.valid?

		@lab.duration = '7:00'
		assert_not @lab.valid?

		@lab.duration = 7.00
		assert_not @lab.valid?

		@lab.duration = 2017-06-28
		assert_not @lab.valid?

		@lab.duration = "2"
		assert_not @lab.valid?
	end

end
