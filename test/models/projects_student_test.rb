require 'test_helper'

class ProjectsStudentTest < ActiveSupport::TestCase
  
	def setup
		@projects_student = projects_students(:one)
	end

	test "empty is not valid" do
		projects_student = ProjectsStudent.new
		assert_not projects_student.valid?
	end

	test "should not save empty projects_student entry" do
		projects_student = ProjectsStudent.new
		assert_not projects_student.save
	end

	test "should not save entry without student_id" do
		@projects_student.student_id = nil
		assert_not @projects_student.save
	end

	test "should not save entry without project_id" do
		@projects_student.project_id = nil
		assert_not @projects_student.save
	end

end
