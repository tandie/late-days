require 'test_helper'

class SemesterTest < ActiveSupport::TestCase
  
	def setup
		@semester = semesters(:one)
	end

	test "should not save empty Semester" do
		semester = Semester.new
		assert_not semester.save
	end

	test "term should be valid" do
		assert @semester.valid?
	end

	test "shoud save valid Semester" do
		assert @semester.save
	end

	test "label should be present" do
		@semester.label = "  "
		assert_not @semester.save
	end

end
