require 'test_helper'

class StudentTest < ActiveSupport::TestCase
  
	def setup
		@student = students(:one)
		@student2 = students(:two)
	end

	test "student should not be empty" do
		student = Student.new
		assert_not student.valid?
	end

	test "name should be present" do
		@student.name = "   "
		assert_not @student.valid?
	end

	test "ais_id should be present" do
		@student.ais_id = nil
		assert_not @student.valid?
	end

	test "ais_id should be number" do
		@student.ais_id = String.new
		assert_not @student.valid?
	end

	test "name should not be number" do
		@student.name = 12345
		assert_not @student.valid?
	end

	test "ais_id should be unique" do
		@student.save
		@student2.ais_id = @student.ais_id
		assert_not @student2.valid?
	end

end
