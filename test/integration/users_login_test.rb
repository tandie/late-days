require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest

  test "login with invalid information" do
    get login_path
    assert_template 'sessions/new'
    post login_path, params: { session: { username: " ", password: " " } }
    assert_not flash.empty?
    assert_template 'sessions/new'
    assert_not is_logged_in?
  end

  test "login with valid information" do
    get login_path
    post login_path, params: { session: { username: 'xvalacha', password: 'MilujemCisco171' } }
    assert flash.empty?
    assert is_logged_in?
  end

end
