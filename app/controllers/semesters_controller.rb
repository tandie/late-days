class SemestersController < ApplicationController
  include SemestersHelper

  def index
    @semester = read_current_semester

    if @semester.nil?
      flash[:danger] = 'Opa, Murphyho zákony zaúradovali. Skúste neskôr'
    else
      if @semester.new_record?
        unless @semester.save
          flash[:danger] = 'Záznamy sa nepodarilo aktualizovať'
        end
      end
    end

    @semesters = Semester.all
  end

  def show
    @semester = Semester.find(params[:id])
    @courses_semesters = CoursesSemester.where(semester_id: params[:id])
  end
end