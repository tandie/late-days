class LabsStudentsImportsController < ApplicationController
  def new
    @labs_students_import = LabsStudentsImport.new
    @lab = Lab.find(params[:lab_id])
    @instructor = Instructor.find(params[:instructor_id])
  end

  def create
    @labs_students_import = LabsStudentsImport.new(params[:labs_students_import])
    @instructor = Instructor.find(params[:instructor_id])
    @lab = Lab.find(params[:lab_id])

    if @labs_students_import.present? && @labs_students_import.save(@lab)
        flash[:success] = 'Záznamy boli importované'
        redirect_to instructor_lab_path(@lab.instructor.ais_id, @lab)
      else
        flash[:danger] = 'Opa, vyskytla sa chyba'
        render :new
      end
    end
end
