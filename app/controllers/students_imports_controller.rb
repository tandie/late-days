class StudentsImportsController < ApplicationController
  def new
    @students_import = StudentsImport.new
  end

  def create
    @students_import = StudentsImport.new(params[:students_import])
    if @students_import.save
      flash[:success] = 'Záznamy boli importované'
      redirect_to students_url
    else
      flash[:danger] = 'Záznamy sa nepodarilo importovať. Skúste znova'
      render :new
    end
  end
end
