class InstructorsController < ApplicationController
  def index
    @instructors = Instructor.all.order(:name)
    @title = 'Cvičiaci'
  end

  def show
    @instructor = Instructor.find(params[:id])
    @courses_semester = CoursesSemester.find_by(params[:courses_semester_id])
    @labs = @instructor.labs
  end
end
