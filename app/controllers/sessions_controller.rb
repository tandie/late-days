class SessionsController < ApplicationController

  include LdapHelper
  include SessionsHelper

  skip_before_action :authorize

  layout false

  def new
    @title = 'Prihlásenie'
  end

  def create
    if (user = ldap_auth(params[:session][:username], params[:session][:password]))
      log_in user
      if user.role.to_s == 'teacher' || user.uid.to_i == 79791
        redirect_to instructor_path(current_user)
      else
        redirect_to student_path(current_user)
      end
    else
      flash.now[:danger] = 'Nesprávne meno alebo heslo'
      render :new
    end
  end

  def destroy
    log_out
    redirect_to login_url
  end
end
