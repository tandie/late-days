class ProjectsController < ApplicationController
  def index
    @title = 'Zadania'
    @projects = Project.all.order(:deadline)
    @courses_semester = CoursesSemester.find(params[:courses_semester_id])
  end

  def show
    @courses_semester = CoursesSemester.find(params[:courses_semester_id])
    @project = Project.find(params[:id])
    @data = ProjectsStudent.where(project_id: @project).group(:spent_late_days).count
  end
end
