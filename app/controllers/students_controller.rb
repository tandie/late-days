class StudentsController < ApplicationController

  include TuringHelper

  def index
    @students = Student.all.order(:name)
    @title = 'Študenti'
  end

  def show
      @title = 'Moje Late Days'
      ais_id = current_user.ais_id
      latedays = get_projects_for_student(ais_id)

      unless latedays.nil?
        @projects = latedays[ais_id.to_s]

        unless @projects.nil?
          @projects.each do |project|
            Project.insert_from_turing_data(project, ais_id, current_semester.id)
          end
        end
      end
  end

  def import
    Student.import(params[:file])
    flash[:success] = 'Študenti pridaní'
    redirect_to students_path
  end

end
