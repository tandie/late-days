class LabsStudentsController < ApplicationController
  def new
  end

  def create
    if current_user.instance_of? Instructor
      flash[:danger] = 'Opa, cvičiaci sa nemôže registrovať na cvičenie'
      redirect_to :back
    end

    @lab = Lab.select(:id).find_by(id: params[:lab_id])

    @lab_student = LabsStudent.find_by(student_id: current_user.ais_id, lab_id: @lab.id)

    if @lab_student.nil?
      @lab_student = LabsStudent.new(student_id: current_user.ais_id, lab_id: @lab.id)

      if !@lab_student.save
        flash[:danger] = 'Opa, nastala chyba. Skúste to znovu.'
      end
    else
      flash[:warning] = 'Už ste boli registrovaní na dané cvičenie.'
    end

    redirect_to :back
  end

  def index
    @labs_students = LabsStudent.where(student_id: params[:student_id])
    @title = 'Moje cvičenia'
  end
end
