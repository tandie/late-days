class CoursesSemestersController < ApplicationController

  include ActionView::Helpers::NumberHelper

  def new
    @courses_semester = CoursesSemester.new
    @title = 'Pridať predmet do semestra'
  end

  def create
    if (@course = CoursesSemester.create_from_course(params, current_semester.id))
      flash.now[:success] = 'Kurz bol pridaný do semestra.'
      redirect_to courses_semester_path(@course)
    else
      flash.now[:danger] = 'Opa, vyskytla sa chyba, skúste to znovu.'
      render :new
    end
  end

  def show
    @courses_semester = CoursesSemester.find(params[:id])
    @projects = @courses_semester.projects
    calculate_success(@projects)
    @labs = @courses_semester.labs.order(:day, :start_at)
    @title = @courses_semester.course.name
  end

  def index
    @courses = CoursesSemester.find_by(semester_id: current_semester)
    @title = current_semester.label
  end

  def calculate_success(projects)
    projects.each do |project|
      projects_students = ProjectsStudent.where(project_id: project)
      spent_late_days = projects_students.group(:spent_late_days).count
      students = projects_students.count
      sum = 0

      spent_late_days.each do |key, value|
        sum += (key+1)*(value)
      end
      res = (students.to_f / sum.to_f) * 100
      project.success = number_with_precision(res, precision: 2)
      project.save
    end
  end

end