class LabsController < ApplicationController
  include LabsHelper

  helper_method :students_lab_project

  def show
    @lab = Lab.find(params[:id])
    @projects = Lab.get_projects(@lab)
  end

  def index
    @lab = Lab.find_by(courses_semester_id: params[:courses_semesters_id])
    @courses_semester = CoursesSemester.find(params[:courses_semester_id])
    @title = @courses_semester.course.name
  end

  def sync
    @course = Course.find(params[:format])
    get_labs @course, current_semester
    redirect_to :back
  end

  def students_lab_project(lab, project)
    LabsStudent.students_for_lab_and_project(lab, project)
  end

end
