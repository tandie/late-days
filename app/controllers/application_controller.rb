class ApplicationController < ActionController::Base
  before_action :authorize

  protect_from_forgery with: :exception

  include SessionsHelper
  include DateTimeHelper
  include SemestersHelper

  helper_method :days_between
  helper_method :format_time
  helper_method :format_date
  helper_method :current_semester
  helper_method :current_user
  helper_method :get_home_url

  private

  def user_home_url
    if current_user.instance_of? Instructor
      instructor_path(current_user.ais_id)
    elsif current_user.instance_of? Student
      student_path(current_user.ais_id)
    end
  end

  def get_home_url
    @home_url ||= user_home_url
  end

  def authorize
    redirect_to login_url unless logged_in?
  end
end
