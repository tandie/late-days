class Import
  include ActiveModel::Model
  attr_accessor :file

  def initialize(attributes = {})
    unless attributes.nil?
      attributes.each do |name, value|
        send("#{name}=", value)
      end
    end
  end

  def persisted
    false
  end

  def open_spreadsheet
    # file.nil? ?
        # raise 'Nebol vybraný žiaden súbor' :
        case File.extname(file.original_filename)
        when '.xls' then
          Roo::Excel.new(file.path)
        when '.xlsx' then
          Roo::Excelx.new(file.path)
        else
          raise "Nepodporovaný typ súboru: #{file.original_filename}"
        end
  end
end
