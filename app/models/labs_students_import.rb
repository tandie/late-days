class LabsStudentsImport < Import
  def save(lab_id)
    if imported_labs_students(lab_id).nil?
      false
    else
      if imported_labs_students(lab_id).map(&:valid?).all?
        imported_labs_students(lab_id).each(&:save!)
        true
      else
        imported_labs_students(lab_id).each_with_index do |student, index|
          student.errors.full_messages.each do |message|
            errors.add :base, "Row #{index + 2}: #{message}"
          end
        end
        false
      end
    end
  end

  def imported_labs_students(lab_id)
    @imported_labs_students ||= load_imported_labs_students(lab_id)
  end

  def load_imported_labs_students(lab_id)
    sheet = open_spreadsheet
    return nil if sheet.nil?
    header = sheet.row(1)
    (2..sheet.last_row).map do |i|
      row = Hash[header.zip sheet.row(i)]
      ais_id = row['ID'].to_i
      name = row['Celé meno'].to_s
      next if ais_id == 'ID'

      student = Student.select(:ais_id).create_with(name: name).find_or_create_by(ais_id: ais_id)
      lab = Lab.select(:id).find(lab_id)

      LabsStudent.find_or_create_by(student_id: student.ais_id, lab_id: lab.id)
    end
  end
end
