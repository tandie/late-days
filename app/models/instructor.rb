class Instructor < ApplicationRecord
  self.primary_key = :ais_id

  has_many :labs
  has_many :courses_semesters, through: :labs, dependent: :delete_all

  validates :name, presence: true, format: {
    without: /[0-9]/, message: 'Meno neobsahuje čísla'
  }

  validates :ais_id, presence: true, uniqueness: true, format: {
    with: /\A\d+\z/, message: 'AIS id obsahuje len čísla'
  }

  def self.get(id)
    Instructor.find_by(ais_id: id)
  end
end
