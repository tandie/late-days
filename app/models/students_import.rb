class StudentsImport < Import
  validates :name, presence: true
  validates :ais_id, presence: true, format: { with: /\A\d+\z/, message: 'AIS id obsahuje len čísla' }

  def save
    if imported_students.nil?
      false
    else
      if imported_students.map(&:valid?).all?
        imported_students.each(&:save!)
        true
      else
        imported_students.each_with_index do |student, index|
          student.errors.full_messages.each do |message|
            errors.add :base, "Row #{index + 2}: #{message}"
          end
        end
        false
      end
    end
  end

  def imported_students
    @imported_students ||= load_imported_students
  end

  def load_imported_students
    sheet = open_spreadsheet
    return nil if sheet.nil?

    header = sheet.row(1)
    (2..sheet.last_row).map do |i|
      row = Hash[header.zip sheet.row(i)]
      ais_id = row['ID'].to_i
      next if ais_id == 'ID'
      name = row['Celé meno'].to_s
      Student.create_with(name: name).find_or_create_by(ais_id: ais_id)
    end
  end
end
