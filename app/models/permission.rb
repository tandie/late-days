class Permission
  def initialize(user)
    allow :sessions, %i[new create destroy]

    if user.instance_of? Student
      allow :dashboard, [:view]
      allow :semesters, [:show]
      allow :courses_semesters, [:show]
      allow :sessions, %i[new create destroy]
      allow :labs_students, %i[index create]
    end

    allow_all if user.instance_of? Instructor
  end

  def allow?(controller, action, resource = nil)
    allowed = @allow_all || @allowed_actions[[controller.to_s, action.to_s]]
    allowed && (allowed == true || resource && allowed.call(resource))
  end

  def allow_all
    @allow_all = true
  end

  def allow(controllers, actions, &block)
    @allowed_actions ||= {}
    Array(controllers).each do |controller|
      Array(actions).each do |action|
        @allowed_actions[[controller.to_s, action.to_s]] = block || true
      end
    end
  end
end
