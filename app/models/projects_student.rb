class ProjectsStudent < ApplicationRecord
  belongs_to :student
  belongs_to :project

  validates :spent_late_days, numericality: { only_integer: true }
end
