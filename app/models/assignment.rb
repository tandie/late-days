class Assignment < ApplicationRecord
  belongs_to :course
  has_many :projects, dependent: :destroy
  has_many :courses_semesters, through: :projects

  validates :name, presence: true
end
