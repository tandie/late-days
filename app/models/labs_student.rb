class LabsStudent < ApplicationRecord
  belongs_to :lab
  belongs_to :student

  def self.students_for_lab_and_project(lab, project)
    students = LabsStudent.select(:student_id).where(lab_id: lab)
    ProjectsStudent.where({ student_id: students, project_id: project })
  end
end
