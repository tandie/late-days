class Project < ApplicationRecord
  belongs_to :assignment
  belongs_to :courses_semester

  has_many :projects_students
  has_many :students, through: :projects_students

  def self.insert_from_turing_data(data, student_id, semester_id)
    course = Course.select(:id).find_or_create_by(abbr: data['course'])
    assignment = Assignment
                     .select(:id)
                     .create_with(course_id: course.id)
                     .find_or_create_by(name: data['assignment'])

    if assignment.new_record?
      assignment.save
    end

    courses_semester = CoursesSemester
                           .select(:id)
                           .find_or_create_by(course_id: course.id, semester_id: semester_id)

    project = Project
                  .select(:id)
                  .create_with(deadline: data['deadline'])
                  .find_or_create_by(assignment_id: assignment.id, courses_semester_id: courses_semester.id)

    if project.new_record?
      project.save
    end

    spent_late_days = days_between(data['submission_date'], data['deadline'])
    project_student = ProjectsStudent
                          .select(:id)
                          .create_with(spent_late_days: spent_late_days.to_i)
                          .find_or_create_by(project_id: project.id, student_id: student_id)

    if project_student.new_record?
        project_student.save
    end
  end

  def self.days_between(start_str, end_str)
    diff = (end_str.to_date - start_str.to_date).to_i

    if diff < 0
      diff *= -1
    else
      diff = 0
    end
  end
end
