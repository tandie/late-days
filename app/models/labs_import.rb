class LabsImport < Import
  include ActiveModel::Model
  attr_accessor :file

  validates :duration, numericality: { only_integer: true }

  def save
    if imported_labs.nil?
      false
    else
      imported_labs.each_with_index do |lab, _courses_semester_id|
        lab.errors.full_message.each do |message|
          errors.add :base, "Row #{index + 2}: #{message}"
        end
      end
    end
  end

  def imported_labs
    @imported_labs ||= load_imported_labs
  end

  #   def load_imported_labs
  #     sheet = open_spreadsheet
  #     return nil unless !sheet.nil?
  #     header = sheet.row(1)
  #     (2..sheet.last_row).map do |i|
  #       row = Hash[header.zip sheet.row(i)]
  #       next if !row["Akcia"].equal("Cvičenie")
  #       instructor = row["Vyučujúci"]
  #       duration = 2
  #       start_from = row["Od"]
  #       day = row["Deň"]
  #       lab = Lab.create_with()
  #   end
end
