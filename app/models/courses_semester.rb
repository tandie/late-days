class CoursesSemester < ApplicationRecord
  belongs_to :course
  belongs_to :semester

  has_many :labs, dependent: :destroy
  has_many :instructors, through: :labs
  has_many :projects, dependent: :destroy
  has_many :assignments, through: :projects

  validates :late_days, numericality: {
    allow_nil: true,
    only_integer: true,
    greater_than_or_equal_to: 1,
    less_than_or_equal_to: 30
  }

  def self.create_from_course(params, semester_id)
    course = Course.get_or_create_course(params)

    @courses_semester = CoursesSemester.new(
        course_id: course.id,
        semester_id: semester_id,
        supervisor: params[:supervisor],
        late_days: params[:late_days])

    if @courses_semester.save
      @courses_semester
    else
      nil
    end
  end
end
