class User

  def initialize(data)
    @data = data
  end

  def uid
    @uid ||= @data[:uisid].first
  end

  def login
    @login ||= @data[:uid].first
  end

  def email
    @email ||= @data[:mail].find { |mail| mail =~ /@stuba.sk\z/ }
  end

  def name
    @name ||= @data[:cn].first 
  end

  def role
    @role ||= ((value = @data[:employeetype].first.to_sym) == :staff ? :teacher : :student)
  end

  def to_params
    {
      ais_uid: uid,
      ais_login: login,
      login: login,
      name: name,
      email: email,
      role: role
    }
  end

end
