class Student < ApplicationRecord
  self.primary_key = :ais_id

  has_many :projects_students
  has_many :projects, through: :projects_students
  has_many :labs_students
  has_many :labs, through: :labs_students

  validates :name, presence: true, format: { without: /[0-9]/, message: 'Meno neobsahuje čísla' }
  validates :ais_id, presence: true, uniqueness: true, format: { with: /\A\d+\z/, message: 'AIS id obsahuje len čísla' }

  def self.import(file)
    sheet = open_spreadsheet(file)
    sheet.row(1)
    sheet.each(ais_id: 'C', name: 'B').each do |hash|
      puts hash.inspect
    end
   end

  def self.open_spreadsheet(file)
    case File.extname(file.original_filename)
    when '.csv' then Csv.new(file.path, nil, :ignore)
    when '.xls' then Excel.new(file.path, nil, :ignore)
    when '.xlsx' then Excelx.new(file.path, nil, :ignore)
    else raise "Neznámy typ súboru: #{file.original_filename}"
    end
  end

  def self.get(id)
    Student.find_by(ais_id: id)
  end
end
