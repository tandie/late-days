class Lab < ApplicationRecord
  enum day: %i[pondelok utorok streda štvrtok piatok]
  belongs_to :instructor
  belongs_to :courses_semester

  validates :duration, numericality: { only_integer: true }

  def self.get_students_for_instructor(instructor_id, semester_id)
    courses = CoursesSemester.where(semester_id: semester_id)

    courses.each do |course|
      Lab.where(instructor_id: instructor_id, course_id: course.id)
    end
  end

  def self.get_projects(lab)
    Project.where(courses_semester_id: lab.courses_semester_id)
  end
end
