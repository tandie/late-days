class Semester < ApplicationRecord
  has_many :courses_semesters
  has_many :courses, through: :courses_semesters

  validates :label, presence: true, uniqueness: true
end
