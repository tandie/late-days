class Course < ApplicationRecord
  has_many :courses_semesters
  has_many :semesters, through: :courses_semesters

  accepts_nested_attributes_for :courses_semesters

  validates :abbr, presence: true, uniqueness: true

  def self.get_or_create_course(params)
    Course.select(:id).create_with(name: params[:name]).find_or_create_by(abbr: params[:abbr])
  end
end
