module BreadcrumbHelper
  def ensure_breadcrumb
    @breadcrumb ||= [{ title: 'Domov', url: home_url }]
  end

  def breadcrumb_add(title, url)
    ensure_breadcrumb << { title: title, url: url }
  end

  def render_breadcrumb
    render partial: 'shared/breadcrumb', locals: { breadcrumb: ensure_breadcrumb }
  end
end
