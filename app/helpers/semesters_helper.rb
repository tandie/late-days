require 'open-uri'

module SemestersHelper
  def read_current_semester
    doc = Nokogiri::HTML(open('http://is.stuba.sk/student/harmonogram.pl?fakulta=70;lang=sk'))
    rows = doc.xpath('//table/tbody/tr/td/small/b')
    current = rows.first.at_xpath('text()').to_s
    @semester = current.split('-').first.strip
    Semester.find_or_create_by(label: @semester)
  end

  def current_semester
    @semester ||= Semester.last
  end
end
