module DateTimeHelper
  def lab_end_time(lab)
    if lab.valid?
      time = lab.duration * 50
      break_time = (lab.duration - 1) * 10
      hours = time / 60
      minutes = (time + break_time) % 60
      ending = lab.start_at + hours.hours + minutes.minutes
      format_time ending
      end
  end

  def format_time(time)
    l time, format: :customformat
  end

  def format_date(date)
    l date, format: :customformat
  end

  def days_between(start_str, end_str)
    diff = (end_str.to_date - start_str.to_date).to_i

    if (diff < 0)
      diff *= -1
    else
      diff = 0
    end
  end
end
