module LabsHelper
  def get_labs(course, semester)
    rows = parse_labs course, semester

    if rows.nil?
      flash[:warning] = 'Nenašli sa nijaké cvičenia pre daný predmet.'
      return
    end

    courses_semester ||= CoursesSemester.select(:id).where(course_id: course, semester_id: semester).first
    labs ||= []

    rows.each do |row|
      day = get_day row.at('td[1]').text.strip
      start = row.at('td[2]').text.strip
      ending = row.at('td[3]').text.strip
      type = row.at('td[5]').text.strip
      duration = calc_duration start, ending
      instr_name = row.at('td[7]').text
      instr_fname = instr_name.split('.').first.strip
      instr_lname = instr_name.split('.').last.strip
      name = instr_fname + '% ' + instr_lname

      instructor = Instructor.select(:ais_id).where('name LIKE ?', name).first

      if instructor.nil?
        href = row.at('td[7] a').attributes['href'].value
        ais = href.slice(href.index('=') + 1..href.index(';') - 1)
        instructor = Instructor.create(ais_id: ais, name: instr_name)
      end

      next unless type == 'Cvičenie'
      start_at = lab_start start

      labs << Lab.where(
        day: day,
        start_at: start_at,
        duration: duration,
        courses_semester_id: courses_semester.id,
        instructor_id: instructor.ais_id
      ).first_or_create
    end
    flash[:success] = 'Cvičenia aktualizované.'
  end

  def parse_labs(course, semester)
    mechanize = Mechanize.new

    url = 'http://is.stuba.sk/katalog/rozvrhy_view.pl?konf=1;f=70;lang=sk'
    page = mechanize.get(url)
    td = page.at("table tbody tr td small:contains('#{semester.label}')")

    return if td.nil?

    row = td.parent

    return if row.nil?

    table = row.parent
    value = table.at('td small input').attributes['value'].value
    form = page.forms.first
    form.checkbox(value: value).check

    page = form.submit
    form = page.forms.first
    select_option form, 'predmet', course.abbr
    select_option form, 'format', 'Zoznam rozvrhových akcii'
    view = form.button_with(name: 'zobraz')

    page = form.submit(view)
    doc = Nokogiri::HTML(page.body)
    table = doc.css('table').first
    table.css('tbody tr')
  end

  def select_option(form, field_name, text)
    value = nil
    form.field_with(name: field_name).options.each do |o|
      if o.text.start_with? text
        value = o
        break
      end
    end
    raise ArgumentError, "No option with text #{text} in field '#{field_name}'" unless value
    form.field_with(name: field_name).value = value
  end

  def get_day(day)
    case day
    when 'Po'
      0
    when 'Ut'
      1
    when 'St'
      2
    when 'Št'
      3
    when 'Pi'
      4
    else
      0
    end
  end

  def calc_duration(start, ending)
    start_hour = start.split('.').first.to_i
    start_minutes = start.split('.').last.to_i

    ending_hour = ending.split('.').first.to_i
    ending_minutes = ending.split('.').last.to_i

    (60 * (ending_hour - start_hour) + (ending_minutes - start_minutes)) / 50
  end

  def lab_start(start)
    hour = start.split('.').first.to_i
    minute = start.split('.').last.to_i
    "#{hour}:#{minute}"
  end
end
