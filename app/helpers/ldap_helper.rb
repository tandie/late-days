require 'timeout'
require 'net/ldap'

module LdapHelper
  def ldap_auth(username, password)
    ldap = Net::LDAP.new(
      host: 'ldap.stuba.sk',
      port: 636,
      base: 'ou=People,dc=stuba,dc=sk',
      encryption: :simple_tls,
      auth: {
        method: :simple,
        username: "uid=#{username},ou=People,dc=stuba,dc=sk",
        password: password
      }
    )

    treebase = 'dc=stuba,dc=sk'
    filter = Net::LDAP::Filter.eq 'uid', username

    begin
      Timeout.timeout(5) do
        begin
          entries = begin
                     ldap.search(base: treebase, filter: filter, return_result: true)
                   rescue StandardError
                     nil
                   end
          user = User.new(entries.first) if entries.present?
        end
      end
    end
  end
end
