module SessionsHelper
  def current_user
    if session[:user_role] == "teacher" || session[:user_uid] == 79791
      @current_user ||= Instructor.get(session[:user_uid])
    elsif session[:user_role] == "student"
      @current_user ||= Student.get(session[:user_uid])
    end
  end

  def check_if_exists(user)
    if user.role.to_s == "teacher" || user.uid.to_i == 79791
      @current_user ||= Instructor.create_with(name: user.name)
                            .find_or_create_by(ais_id: user.uid)
    elsif user.role.to_s == "student"
      @current_user ||= Student.create_with(name: user.name)
                            .find_or_create_by(ais_id: user.uid)
    end

    if @current_user.new_record?
      @current_user.save
    end
  end

  def logged_in?
    !current_user.nil?
  end

  def log_in(user)
    session[:user_uid] = user.uid.to_i
    session[:user_role] = user.role.to_s
    puts session[:user_uid]
    puts session[:user_role]
    check_if_exists user
  end

  def log_out
    session.delete(:user_uid)
    session.delete(:user_role)
    @current_user = nil
  end
end
