require 'net/http'
require 'json'

module TuringHelper
  def get_projects_for_student(student_id)
    url = URI.parse('http://faketuring.wz.sk/index.php')
    params = { student_id: student_id }

    http = Net::HTTP.new(url.host, url.port)
    request = Net::HTTP::Post.new(url.path, {'Content-Type' => 'application/json'})
    request.body = params.to_json

    response = http.request(request)

    if valid_json?(response.body)
      JSON.parse(response.body)
    end
  end

  def valid_json?(string)
    begin
      !!JSON.parse(string)
    rescue JSON::ParserError
      false
    end
  end
end
