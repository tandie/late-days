Rails.application.routes.draw do
  get     '/home',                      to: 'student#show'
  get     '/login',                     to: 'sessions#new'
  post    '/login',                     to: 'sessions#create'
  delete  '/logout',                    to: 'sessions#destroy'
  get     '/labs/sync',                 to: 'labs#sync',  as: :sync_labs
  root    'sessions#new'

  resources :semesters, only: [:show, :index]

  resources :courses_semesters, only: [:show, :index, :new, :create] do
    resources :labs, only: [:show, :index] do
      resources :labs_students, only: [:index, :show]
    end
    resources :projects, only: [:index, :show]
  end

  resources :instructors, only: [:index, :show] do
    resources :labs, only: [:index, :show] do
      resources :labs_students_imports, only: [:new, :create]
    end
  end

  resources :students, only: [:index, :show] do
    resources :labs do
      resources :labs_students, only: [:create]
    end
  end

  resources :students_imports, only: [:new, :create]
  resources :labs_imports, only: [:new, :create]
end
