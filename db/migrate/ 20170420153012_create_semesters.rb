class CreateSemesters < ActiveRecord::Migration[5.0]
  def change
    create_table :semesters do |t|
      t.string :label
    end
    add_index :semesters, :label, unique: true
  end
end