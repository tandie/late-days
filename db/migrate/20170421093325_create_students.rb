class CreateStudents < ActiveRecord::Migration[5.0]
  def change
    create_table :students, id: false do |t|
      t.primary_key :ais_id
      t.string :name
    end
    add_index :students, :name
  end
end
