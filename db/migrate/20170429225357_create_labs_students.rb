class CreateLabsStudents < ActiveRecord::Migration[5.0]
  def change
    create_table :labs_students do |t|
      t.references :student
      t.references :lab
    end
  end
end
