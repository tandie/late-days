class CreateInstructors < ActiveRecord::Migration[5.0]
  def change
    create_table :instructors, id: false do |t|
      t.primary_key :ais_id
      t.string :name
    end
    add_index :instructors, :name
  end
end
