class CreateProjectsStudents < ActiveRecord::Migration[5.0]
  def change
    create_table :projects_students do |t|
    	t.references :project, index: true    	
    	t.references :student, index: true
    	t.integer :spent_late_days
    end
  end
end
