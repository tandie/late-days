class CreateCoursesSemesters < ActiveRecord::Migration[5.0]
  def change
    create_table :courses_semesters do |t|
      t.references :course
      t.references :semester
      t.string :supervisor
      t.integer :late_days
    end
  end
end