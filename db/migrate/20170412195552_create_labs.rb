class CreateLabs < ActiveRecord::Migration[5.0]
  def change
    create_table :labs do |t|
      t.references :instructor, index: true
      t.references :course, index: true
      t.time :start_at
      t.integer :day
    end
    add_index :labs, [:day, :start_at]
  end
end
