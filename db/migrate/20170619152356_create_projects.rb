class CreateProjects < ActiveRecord::Migration[5.0]
  def change
  	create_table :projects do |t|
  		t.references :assignment, foreign_key: true
  		t.references :courses_semester, foreign_key: true
  		t.datetime :deadline
  		t.decimal :success
  	end
  end
end
