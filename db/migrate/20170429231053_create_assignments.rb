class CreateAssignments < ActiveRecord::Migration[5.0]
  def change
    create_table :assignments do |t|
      t.references :course, index: true
      t.string :name
    end
    add_index :assignments, :name
  end
end
