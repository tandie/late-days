class CreateCourses < ActiveRecord::Migration[5.0]
  def change
    create_table :courses do |t|
      t.string :name
      t.string :abbr
      t.integer :latedays
    end
    add_index :courses, :abbr, unique: true
  end
end
