# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170619152952) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "assignments", force: :cascade do |t|
    t.integer "course_id"
    t.string  "name"
    t.index ["course_id"], name: "index_assignments_on_course_id", using: :btree
    t.index ["name"], name: "index_assignments_on_name", using: :btree
  end

  create_table "courses", force: :cascade do |t|
    t.string "name"
    t.string "abbr"
    t.index ["name"], name: "index_courses_on_name", unique: true, using: :btree
  end

  create_table "courses_semesters", force: :cascade do |t|
    t.integer "course_id"
    t.integer "semester_id"
    t.string  "supervisor"
    t.integer "late_days"
    t.index ["course_id"], name: "index_courses_semesters_on_course_id", using: :btree
    t.index ["semester_id"], name: "index_courses_semesters_on_semester_id", using: :btree
  end

  create_table "instructors", primary_key: "ais_id", force: :cascade do |t|
    t.string "name"
    t.index ["name"], name: "index_instructors_on_name", using: :btree
  end

  create_table "labs", force: :cascade do |t|
    t.integer "instructor_id"
    t.integer "courses_semester_id"
    t.time    "start_at"
    t.integer "duration"
    t.integer "day"
    t.index ["courses_semester_id"], name: "index_labs_on_courses_semester_id", using: :btree
    t.index ["day", "start_at"], name: "index_labs_on_day_and_start_at", using: :btree
    t.index ["instructor_id"], name: "index_labs_on_instructor_id", using: :btree
  end

  create_table "labs_students", force: :cascade do |t|
    t.integer "student_id"
    t.integer "lab_id"
    t.index ["lab_id"], name: "index_labs_students_on_lab_id", using: :btree
    t.index ["student_id"], name: "index_labs_students_on_student_id", using: :btree
  end

  create_table "projects", force: :cascade do |t|
    t.integer "assignment_id"
    t.integer "courses_semester_id"
    t.date    "deadline"
    t.decimal "success"
    t.index ["assignment_id"], name: "index_projects_on_assignment_id", using: :btree
    t.index ["courses_semester_id"], name: "index_projects_on_courses_semester_id", using: :btree
    t.index ["deadline"], name: "index_projects_on_deadline", using: :btree
  end

  create_table "projects_students", force: :cascade do |t|
    t.integer "project_id"
    t.integer "student_id"
    t.integer "spent_late_days"
    t.index ["project_id"], name: "index_projects_students_on_project_id", using: :btree
    t.index ["student_id"], name: "index_projects_students_on_student_id", using: :btree
  end

  create_table "semesters", force: :cascade do |t|
    t.string "label"
    t.index ["label"], name: "index_semesters_on_label", unique: true, using: :btree
  end

  create_table "students", primary_key: "ais_id", force: :cascade do |t|
    t.string "name"
    t.index ["name"], name: "index_students_on_name", using: :btree
  end

  add_foreign_key "projects", "assignments"
  add_foreign_key "projects", "courses_semesters"
end
